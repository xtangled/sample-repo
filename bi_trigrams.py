import nltk.collocations 

text = str(input())
tokens = nltk.word_tokenize(text)
output_bi = list(nltk.bigrams(tokens))
print(output_bi)

output_tri = list(nltk.trigrams(tokens))
print(output_tri)